# (Bug Title)

## What was the expected outcome?

(Expectation)

## What was the actual outcome?

(Reality)

## System Details

+ Operating system:
+ Version:

## Steps to Reproduce

1.  Do a thing
2.  Do another thing
3.  Do breaking thing

Link to error log: